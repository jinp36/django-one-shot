from django.urls import path
from todos.views import (
    todo_list_list,
    todo_list_detail,
    create_list,
    delete_list,
    create_item,
    edit_item,
    todo_list_update,
)

urlpatterns = [
    path("todos/", todo_list_list, name="todo_list_list"),
    path("todos/<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("todos/create/", create_list, name="todo_list_create"),
    path("todos/<int:id>/edit/", todo_list_update, name="todo_list_update"),
    path("todos/<int:id>/delete/", delete_list, name="todo_list_delete"),
    path("todos/items/create/", create_item, name="todo_item_create"),
    # path("todos/items/", item_detail, name="item_detail"),
    path("todos/items/<int:id>/edit/", edit_item, name="todo_item_update"),
]
