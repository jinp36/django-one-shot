from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import ListForm, ItemForm


def todo_list_list(request):
    todo_list = TodoList.objects.all()

    context = {"todo_list_list": todo_list}
    return render(request, "todos/todo_list_list.html", context)


def todo_list_detail(request, id):
    todo_item = get_object_or_404(TodoList, id=id)

    context = {"todo_list_detail": todo_item}
    return render(request, "todos/todo_list_detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = ListForm(request.POST)
        if form.is_valid():
            instance = form.save()
            return redirect("todo_list_detail", id=instance.id)
    else:
        form = ListForm()
    context = {"form": form}
    return render(request, "todos/todo_list_create.html", context)


def create_item(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = ItemForm()
    context = {"form": form}
    return render(request, "todos/todo_item_create.html", context)


def todo_list_update(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = ListForm(request.POST, instance=todolist)
        if form.is_valid():
            instance = form.save()
            return redirect("todo_list_detail", id=instance.id)
    else:
        form = ListForm(instance=todolist)

    context = {
        "form": form,
    }
    return render(request, "todos/todo_list_update.html", context)


def edit_item(request, id):
    todo_item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.list.id)
    else:
        form = ItemForm(instance=todo_item)
    context = {"form": form}
    return render(request, "todos/todo_item_update.html", context)


def delete_list(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")

    return render(request, "todos/todo_list_delete.html")
